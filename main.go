package main

import (
	"fmt"
)

const DEBUGMODE = true

func debugPrint(format string, args ...interface{}) {
	if DEBUGMODE {
		fmt.Printf(format, args...)
	}
}

type Backend interface {
	Eval(interface{}) interface{}
}

func main() {

}
