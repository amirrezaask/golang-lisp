package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCharClassify(t *testing.T) {
	class, err := charClass('a')
	assert.NoError(t, err)
	assert.Equal(t, Alphabet, class)

	class, err = charClass('A')
	assert.NoError(t, err)
	assert.Equal(t, Alphabet, class)

	class, err = charClass('0')
	assert.NoError(t, err)
	assert.Equal(t, Numbers, class)
}
