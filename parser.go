package main

import "fmt"

const (
	Alphabet    = "alphabet"
	Numbers     = "numbers"
	DoubleQuote = "doublequote"
	OpenParen   = "openparen"
	CloseParen  = "closeparen"
	Space       = "space"
)

func charClass(c rune) (string, error) {
	if c >= 'A' && c <= 'z' {
		return Alphabet, nil
	} else if c >= '0' && c <= '9' {
		return Numbers, nil
	} else if c == '(' {
		return OpenParen, nil
	} else if c == ')' {
		return CloseParen, nil
	} else if c == '"' {
		return DoubleQuote, nil
	} else if c == ' ' {
		return Space, nil
	} else if c == '(' {
		return OpenParen, nil
	} else if c == ')' {
		return CloseParen, nil
	}
	return "", fmt.Errorf("Character '%c' not supported", c)
}

type Atom struct {
	typ   string
	value string
}

func (a Atom) String() string {
	return fmt.Sprintf("type => %s, value => %s", a.typ, a.value)
}
func readString(code string, start int) (Atom, int) {
	lastIdx := 0
	if code[start] != '"' {
		panic("string should start with '\"' ")
	}

	str := ""
	idx := start
	for {
		if idx >= len(code)-1 {
			lastIdx = idx
			break
		}
		if code[idx] == '"' && idx != start {
			lastIdx = idx
			break
		}
		if code[idx] == '"' && idx == start {
			idx++
			continue
		}
		str += string(code[idx])
		idx++
	}
	return Atom{typ: "string", value: str}, lastIdx
}

func readNumber(code string, start int) (Atom, int) {
	number := ""
	lastIdx := 0

	idx := start
	for {
		if idx >= len(code)-1 {
			lastIdx = idx
			break
		}

		if code[idx] == ')' || code[idx] == '(' {
			lastIdx = idx - 1
			break
		}
		if code[idx] == ' ' {
			lastIdx = idx
			break
		}
		if !(code[idx] >= '0' && code[idx] <= '9') {
			panic("Number is just number")
		}
		number += string(code[idx])
		idx++
	}
	return Atom{value: number, typ: "number"}, lastIdx
}

func readSymbol(code string, start int) (Atom, int) {
	sym := ""
	lastIdx := 0

	idx := start
	for {
		if idx >= len(code)-1 {
			lastIdx = idx
			break
		}
		if code[idx] == ')' || code[idx] == '(' {
			lastIdx = idx - 1
			break
		}
		if code[idx] == '"' {
			panic("symbols can't contain \"")
		}
		if code[idx] == ' ' {
			lastIdx = idx
			break
		}
		sym += string(code[idx])
		idx++
	}
	return Atom{value: sym, typ: "symbol"}, lastIdx
}

type List []interface{}

func readList(code string, start int) (List, int) {
	list := []interface{}{}
	if code[start] != '(' {
		panic("List should start with (")
	}
	if code[len(code)-1] != ')' {
		panic("list should close")
	}

	idx := start
	lastIdx := 0
	for {
		if idx == start && code[idx] != '(' {
			panic("list should start with (")
		}
		if idx >= len(code)-1 {
			break
		}
		if code[idx] == '(' && idx == start {
			idx++
			continue

		}
		if code[idx] == ')' && idx == len(code)-1 {
			lastIdx = idx
			break
		}
		class, err := charClass(rune(code[idx]))
		if err != nil {
			panic(err)
		}
		if class == Space {
			idx++
			continue
		} else if class == Alphabet {
			sym, lastIdx := readSymbol(code, idx)
			list = append(list, sym)
			idx = lastIdx + 1
			continue
		} else if class == Numbers {
			sym, lastIdx := readNumber(code, idx)
			list = append(list, sym)
			idx = lastIdx + 1
			continue
		} else if class == DoubleQuote {
			sym, lastIdx := readString(code, idx)
			list = append(list, sym)
			idx = lastIdx + 2
			continue
		} else if class == OpenParen {
			sym, lastIdx := readList(code, idx)
			list = append(list, sym)
			idx = lastIdx + 1
			continue
		} else if class == CloseParen {
			lastIdx = idx
			break
		}
		break
	}
	return list, lastIdx
}
