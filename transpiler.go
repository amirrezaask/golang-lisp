package main

import (
	"fmt"
	"reflect"
)

// general rule for transpiler => if given symbol is in the special forms table do that thing else just assume thats a golang function call
type GoTranspiler struct {
}
type SpecialForm func(args ...string) string

var GoTranspilerSymbolTable = map[string]SpecialForm{
	"def": func(args ...string) string {
		if len(args) < 2 {
			panic("def needs 2 arguments name and value")
		}
		return fmt.Sprintf("var %s = %s", args[0], args[1])
	},
}

type GoFile struct {
	Package string
	Imports []string
	Vars    []string
	Consts  []string
	Decls   []string
}

func (t *GoTranspiler) Eval(code interface{}) interface{} {
	if reflect.TypeOf(code).Kind() == reflect.Slice {

	} else {
		a := code.(Atom)
		switch a.typ {
		case "string":
			return a.value
		case "symbol":

		}

	}
}
